#! /usr/bin/env python3
import subprocess
from collections import namedtuple
import os

#os.environ["TMPDIR"] = '/tmp/scratch/damcoutinho2'

Time = namedtuple("Time", "d h m s")

t = Time(0, 0, 15, 0)
cpus_per_task = 1
nodes = 1
n_tasks_per_node = 1
for k in range(29, 35):
    size = 2 ** k
    for repetion in range(1, 4):
        rc = subprocess.call(
            ['sbatch', '--job-name=PISeq', '--partition=test', '--mem=100000',
             '--output=../results/seq/%j' + '_' + str(size) + '_' + str(repetion) + '.json',
             '--error=../error/seq/%j' + '_seq' + str(size) + '_rep' + str(repetion) + '.err',
             '--nodes=' + str(nodes), '--ntasks-per-node=' + str(n_tasks_per_node),
             '--cpus-per-task=' + str(cpus_per_task),
             '--time=' + str(t.d) + '-' + str(t.h) + ':' + str(t.m) + ':' + str(t.s), '--exclusive',
             '--mail-user=ilovebasketball.d@gmail.com', '--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90',
             '../sh/seq.sh', str(size)])

# subprocess.check_output -- check the command
