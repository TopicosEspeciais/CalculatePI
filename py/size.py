#! /usr/bin/env python3
import subprocess
from collections import namedtuple
import os

# os.environ["TMPDIR"] = '/tmp/scratch/damcoutinho2'

Time = namedtuple("Time", "d h m s")

i_minimun = 1;
cpus_per_task = 1

for k in range(29, 35):
    n = 2 ** k
    for nodes in [1, 2, 4, 6]:
        if nodes != 1:
            i_minimun = 5
        else:
            i_minimun = 1
        for i in range(i_minimun, 6):
            n_tasks_per_node = 2 ** i
            size_per_task = n / (n_tasks_per_node * nodes)
            for repetion in range(1, 4):
                if (nodes * n_tasks_per_node) <= 32:
                    t = Time(0, 0, 20, 0)
                elif (nodes * n_tasks_per_node) <= 64:
                    t = Time(0, 0, 16, 0)
                elif (nodes * n_tasks_per_node) <= 128:
                    t = Time(0, 0, 12, 0)
                else:
                    t = Time(0, 0, 10, 0)
                rc = subprocess.call(
                    ['sbatch', '--job-name=PISize', '--mem=100000',
                     '--output=../results/size/%j' + '_' + str(nodes) + '_' + str(n_tasks_per_node) + '_' + str(
                         cpus_per_task) + '_' + str(n) + '_' + str(repetion) + '.json',
                     '--error=../error/size/%j' + '_n' + str(nodes) + '_t' + str(n_tasks_per_node) + '_cpu' + str(
                         cpus_per_task) + '_size' + str(n) + '_rep' + str(repetion) + '.err',
                     '--nodes=' + str(nodes), '--ntasks-per-node=' + str(n_tasks_per_node),
                     '--cpus-per-task=' + str(cpus_per_task),
                     '--time=' + str(t.d) + '-' + str(t.h) + ':' + str(t.m) + ':' + str(t.s), '--exclusive',
                     '--mail-user=ilovebasketball.d@gmail.com', '--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90',
                     '../sh/size.sh', str(size_per_task)])
                # print(sum)
                # '--mail-user=ilovebasketball.d@gmail.com','--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90'
                # subprocess.check_output -- check the command
