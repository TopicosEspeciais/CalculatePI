#! /usr/bin/env python3
import subprocess
from collections import namedtuple
import os

#os.environ["TMPDIR"] = '/tmp/scratch/damcoutinho2'

Time = namedtuple("Time", "d h m s max")
time = [Time(0, 0, 0, 35, 30), Time(0, 0, 1, 5, 60), Time(0, 0, 1, 35, 90), Time(0, 0, 2, 5, 120)]

n_tasks_per_node = 32
cpus_per_task = 1

for nodes in [1, 2, 4, 6]:
    for t in time:
        for repetion in range(1, 4):
            rc = subprocess.call(
                ['sbatch', '--job-name=PIPre', '--partition=test', '--mem=100000',
                 '--output=../results/precision/%j' + '_' + str(nodes) + '_' + str(n_tasks_per_node) + '_' + str(
                     cpus_per_task) + '_' + str(t.max) + '_' + str(repetion) + '.json',
                 '--error=../error/precision/%j' + '_n' + str(nodes) + '_t' + str(n_tasks_per_node) + '_cpu' + str(
                     cpus_per_task) + '_t' + str(t.max) + '_rep' + str(repetion) + '.err',
                 '--nodes=' + str(nodes), '--ntasks-per-node=' + str(n_tasks_per_node),
                 '--cpus-per-task=' + str(cpus_per_task),
                 '--time=' + str(t.d) + '-' + str(t.h) + ':' + str(t.m) + ':' + str(t.s), '--exclusive',
                 '--mail-user=ilovebasketball.d@gmail.com', '--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90',
                 '../sh/precision.sh', str(t.max)])

# '--mail-user=ilovebasketball.d@gmail.com','--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90'
# subprocess.check_output -- check the command
