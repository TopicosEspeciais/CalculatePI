#! /usr/bin/env python3
import subprocess
from collections import namedtuple

Time = namedtuple("Time", "d h m s")
t = Time(0,0,5,0)
i_minimun = 1;
cpus_per_task = 1
nodes=1
n_tasks_per_node=5
size_per_task=200
n=17179869184
repetion=1


rc = subprocess.call(
	['sbatch','--job-name=PISize','--partition=test','--mem=100000',
	'--output=../results/size/%j'+'_'+str(nodes)+'_'+str(n_tasks_per_node)+'_'+str(cpus_per_task)+'_'+str(n)+'_'+str(repetion)+'.json',
	'--error=../error/size/%j'+'_n'+str(nodes)+'_t'+str(n_tasks_per_node)+'_cpu'+str(cpus_per_task)+'_size'+str(n)+'_rep'+str(repetion)+'.err',
	'--nodes='+str(nodes),'--ntasks-per-node='+str(n_tasks_per_node),'--cpus-per-task='+str(cpus_per_task),
	'--time='+str(t.d)+'-'+str(t.h)+':'+str(t.m)+':'+str(t.s),'--exclusive',
	'--mail-user=ilovebasketball.d@gmail.com','--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90',
	'../sh/size.sh',str(size_per_task)])


 
# '--mail-user=ilovebasketball.d@gmail.com','--mail-type=ALL,TIME_LIMIT_50,TIME_LIMIT_90'
#subprocess.check_output -- check the command