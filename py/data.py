#! /usr/bin/env python3
import json
import os
import re
from statistics import median
from operator import itemgetter
from pprint import pprint

# CONSTANTS
CONST_PRE = 'precision'
CONST_SIZ = 'size'
CONST_SEQ = 'seq'
CONST_ID = 'id'
CONST_NODES = 'nodes'
CONST_NTASKS = 'n_tasks_per_node'
CONST_CPUTASKS = 'cpus_per_task'
CONST_MAX = 'max'
CONST_REP = 'repetition'
CONST_PI = 'pi'

CONST_HOME = os.environ['HOME']
CONST_PATH_RESULTS = '/Projects/CalculatePI/results/'
CONST_PATH_ERRORS = '/Projects/CalculatePI/error/'
CONST_PATH_RESULTS_PRE = CONST_HOME + CONST_PATH_RESULTS + CONST_PRE
CONST_PATH_RESULTS_SIZ = CONST_HOME + CONST_PATH_RESULTS + CONST_SIZ
CONST_PATH_RESULTS_SEQ = CONST_HOME + CONST_PATH_RESULTS + CONST_SEQ

CONST_PATH_ERROS_PRE = CONST_HOME + CONST_PATH_ERRORS + CONST_PRE
CONST_PATH_ERROS_SIZ = CONST_HOME + CONST_PATH_ERRORS + CONST_SIZ
CONST_PATH_ERROS_SEQ = CONST_HOME + CONST_PATH_ERRORS + CONST_SEQ


class ParseResults:
    lprecision = []
    lsize = []
    lseq =[]
    errors = dict(precision=dict(), size=dict(), seq=dict())

    def parse_precision(self):
        for filename in os.listdir(CONST_PATH_RESULTS_PRE):
            if re.split(r'[_.]', filename)[1] == 'tar':
                continue
            with open(CONST_PATH_RESULTS_PRE + '/' + filename) as f:

                info = re.split(r'[_.]', filename)

                try:
                    data = json.load(f)
                except ValueError:
                    data = dict(numTasks=-1, time_max=-1, pointInside=-1, pointGlobal=-1, pi=-1)
                    self.errors[CONST_PRE][info[0]] = []

                data[CONST_ID] = info[0]
                data[CONST_NODES] = info[1]
                data[CONST_NTASKS] = info[2]
                data[CONST_CPUTASKS] = info[3]
                data[CONST_MAX] = info[4]
                data[CONST_REP] = info[5]
                self.lprecision.append(data)
                self.lprecision = sorted(self.lprecision, key=itemgetter(CONST_ID))

    def parse_size(self):
        for filename in os.listdir(CONST_PATH_RESULTS_SIZ):
            if re.split(r'[_.]', filename)[1] == 'tar':
                continue
            with open(CONST_PATH_RESULTS_SIZ + '/' + filename) as f:

                info = re.split(r'[_.]', filename)

                try:
                    data = json.load(f)
                except ValueError:
                    data = dict(time=-1, pi=-1, numTasks=-1)
                    self.errors[CONST_SIZ][info[0]] = []

                data[CONST_ID] = info[0]
                data[CONST_NODES] = info[1]
                data[CONST_NTASKS] = info[2]
                data[CONST_CPUTASKS] = info[3]
                data[CONST_SIZ] = info[4]
                data[CONST_REP] = info[5]
                self.lsize.append(data)
                self.lsize = sorted(self.lsize, key=itemgetter(CONST_ID))

    def parse_seq(self):
        for filename in os.listdir(CONST_PATH_RESULTS_SEQ):
            if re.split(r'[_.]', filename)[1] == 'tar':
                continue
            with open(CONST_PATH_RESULTS_SEQ + '/' + filename) as f:

                info = re.split(r'[_.]', filename)

                try:
                    data = json.load(f)
                except ValueError:
                    data = {"time": -1, "pointInside": -1, "pointGlobal": -1,
                            "pi": -1}
                    self.errors[CONST_SEQ][info[0]] = []

                data[CONST_ID] = info[0]
                data[CONST_SIZ] = info[1]
                data[CONST_REP] = info[2]
                self.lseq.append(data)
                self.lseq = sorted(self.lseq, key=itemgetter(CONST_ID))

    def check_pre_errors(self):
        for filename in os.listdir(CONST_PATH_ERROS_PRE):
            if re.split(r'[_.]', filename)[1] == 'tar':
                continue
            with open(CONST_PATH_ERROS_PRE + '/' + filename) as f:
                txt = f.read()
                if txt != '':
                    info = re.split('_', filename)
                    self.errors[CONST_PRE][info[0]] = [CONST_PATH_ERROS_PRE + '/' + filename, txt]

    def check_siz_errors(self):
        for filename in os.listdir(CONST_PATH_ERROS_SIZ):
            if re.split(r'[_.]', filename)[1] == 'tar':
                continue
            with open(CONST_PATH_ERROS_SIZ + '/' + filename) as f:
                txt = f.read()
                if txt != '':
                    info = re.split('_', filename)
                    self.errors[CONST_SIZ][info[0]] = [CONST_PATH_ERROS_SIZ + '/' + filename, txt]

    def check_seq_errors(self):
        for filename in os.listdir(CONST_PATH_ERROS_SEQ):
            if re.split(r'[_.]', filename)[1] == 'tar':
                continue
            with open(CONST_PATH_ERROS_SEQ + '/' + filename) as f:
                txt = f.read()
                if txt != '':
                    info = re.split('_', filename)
                    self.errors[CONST_SEQ][info[0]] = [CONST_PATH_ERROS_SEQ + '/' + filename, txt]


class ParallelStats:
    lmedian_by_key = dict()

    def median_by_key(self, key, data):
        if key not in self.lmedian_by_key:
            self.lmedian_by_key.update({key: []})

        for i in range(0, len(data), 3):
            m = median([data[i][key], data[i + 1][key], data[i + 2][key]])
            self.lmedian_by_key[key].append(m)

ps = ParseResults()
ps.parse_seq()
ps.check_seq_errors()
ps.check_siz_errors()
ps.check_pre_errors()
pprint(ps.lseq)