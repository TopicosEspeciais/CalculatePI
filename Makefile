############################# Makefile ##########################
CODE = src/
BIN  = bin/
RESU = results/
ERR  = error/


all: pre size seq

pre:	$(CODE)calculatepi_precision.c
			mpicxx $(CODE)calculatepi_precision.c -o $(BIN)calculatepi_pre
size:	$(CODE)calculatepi_size.c
			mpicxx $(CODE)calculatepi_size.c -o $(BIN)calculatepi_size
seq:	$(CODE)calculatepi_seq.c
			g++ $(CODE)calculatepi_seq.c -lrt -lm -o $(BIN)calculatepi_seq
cpre:
			rm -rf  $(BIN)calculatepi_pre
cres: 
			rm -rf $(RESU)/precision/* $(RESU)/size/*
cerr: 
			rm -rf $(ERR)/precision/* $(ERR)/size/*
cfiles: cres cerr	
call: cpre cfiles

