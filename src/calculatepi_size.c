/*
 * File:   main.cpp
 * Author: demetrios
 *
 */
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../../rapidjson/document.h"
#include "../../rapidjson/writer.h"
#include "../../rapidjson/stringbuffer.h"

/*
 *Main
 */
int main(int argc, char **argv) {

    char hostname[MPI_MAX_PROCESSOR_NAME];

	double pi, initial_time=0, final_time=0;

    int numTasks, rank, rc,len;
    //Tempo máximo em execução
    double piTotal=0;
    //variavel usada para contar a qtd de pontos de cada processo,h usado para
    //calcular a qtd de pontos dentro do circulo
    unsigned long long count = 0, points = 0;
    //qtd de pontos a ser processado de cada processo
    unsigned long long nprocess = atol(argv[1]);

    rc = MPI_Init(&argc, &argv);

	if (rc != MPI_SUCCESS) {
        printf("Error starting MPI program.\n");
        MPI_Abort(MPI_COMM_WORLD, rc);
    }

    

    MPI_Get_processor_name(hostname, &len);

    MPI_Comm_size(MPI_COMM_WORLD, &numTasks);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    initial_time = MPI_Wtime();

    uint seed = time(NULL)*rank;
    for (points = 1; points < nprocess; points++) {

        double x_coord = (double) (rand_r(&seed)) / RAND_MAX;
        double y_coord = (double) (rand_r(&seed)) / RAND_MAX;

        if ((x_coord * x_coord + y_coord * y_coord) <= 1)
            count++;
	}

    pi = 4*((double)count)/((double)nprocess);

    //reduce dos pontos dentro do circulo
    MPI_Reduce(&pi, &piTotal, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    final_time = MPI_Wtime();

    if (rank == 0){

        // 1. Parse a JSON string into DOM.
        const char* json = "{\"time\":0.0,\"pi\":0.0,\"numTasks\":0.0}";
        rapidjson::Document d;
        d.Parse(json);

        // 2. Modify it by DOM.
        rapidjson::Value& s = d["time"];
        s.SetDouble(final_time - initial_time);
        rapidjson::Value& s2 = d["pi"];
        s2.SetDouble(piTotal/numTasks);
        rapidjson::Value& s3 = d["numTasks"];
        s3.SetInt(numTasks);        

        // 3. Stringify the DOM
        rapidjson::StringBuffer buffer;
        rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
        d.Accept(writer);

        printf("%s\n",buffer.GetString());
                 
    }
    
    MPI_Finalize();

    return 0;
}
