/*
 * File:   main.cpp
 * Author: demetrios
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "../../rapidjson/document.h"
#include "../../rapidjson/writer.h"
#include "../../rapidjson/stringbuffer.h"

/*
 *Função responsável por calcular a diferença dos dois tempos
 *recebe como paramentro dois timespec e calcula a diferença entre eles
 */
struct timespec My_diff(struct timespec start, struct timespec end) {
    struct timespec temp;
    //Caso a diferença em nanosegundos for negativo, então deve-se somar um segundo
    //no campo de nanosegundos e pedir emprestado um segundo no campo de segundos.
    //Como se fosse uma subtração onde voce pede emprestado '1' do proximo numero mais
    //importante.
    if ((end.tv_nsec - start.tv_nsec) < 0) {
        temp.tv_sec = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;
}
/*
 *Main
 */
int main(int argc, char **argv) {


    //variavel usada para contar a qtd de pontos de cada processo,h usado para
    //calcular a qtd de pontos dentro do circulo
    unsigned long long count = 0, points = 0;
    //qtd de pontos a ser processado de cada processo
    unsigned long long n = atol(argv[1]);
    //variáveis usadas para pegar o tempo
    struct timespec timeInit, timeEnd;

    if (clock_gettime(CLOCK_REALTIME, &timeInit)) {
        perror("clock gettime");
        exit(EXIT_FAILURE);
    }
	
	srand(time(NULL));
    for (points = 1; points < n; points++) {
        double x_coord = (double) (rand()) / RAND_MAX;
        double y_coord = (double) (rand()) / RAND_MAX;

        if ((x_coord * x_coord + y_coord * y_coord) <= 1)
            count++;
	}

    double pi = 4*((double)count)/((double)n);

    //pegar tempo final
    if (clock_gettime(CLOCK_REALTIME, &timeEnd)) {
        perror("clock gettime");
        exit(EXIT_FAILURE);
    }

    //constante equivalente a segundos em nanosegundos
    double ONE_SECOND_IN_NANOSECONDS = 1000000000;
    //calculando a  diferença entre os dois tempos
    struct timespec timeDiff;
    timeDiff = My_diff(timeInit, timeEnd);
    //somando o tempo em segundo mais o tempo restante em nanosegundos.
    double processTime = (timeDiff.tv_sec + (double) timeDiff.tv_nsec / ONE_SECOND_IN_NANOSECONDS);


    // 1. Parse a JSON string into DOM.
    const char* json = "{\"time\":0.0,\"pointInside\":0.0,\"pointGlobal\":0.0,\"pi\":0.0}";
    rapidjson::Document d;
    d.Parse(json);

    // 2. Modify it by DOM.
    rapidjson::Value& s = d["time"];
    s.SetDouble(processTime);
    rapidjson::Value& s2 = d["pointInside"];
    s2.SetUint64(count);
    rapidjson::Value& s3 = d["pointGlobal"];
    s3.SetUint64(n);
    rapidjson::Value& s4 = d["pi"];
    s4.SetDouble(pi);                

    // 3. Stringify the DOM
    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    d.Accept(writer);

    printf("%s\n",buffer.GetString());
 
    return 0;
}
