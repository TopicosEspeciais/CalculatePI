/*
 * File:   main.cpp
 * Author: demetrios
 *
 */
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../../rapidjson/document.h"
#include "../../rapidjson/writer.h"
#include "../../rapidjson/stringbuffer.h"


/*
 *Main
 */
int main(int argc, char **argv) {

	char hostname[MPI_MAX_PROCESSOR_NAME];

	double pi,lapse_time, initial_time;

    int numTasks, rank, rc, len;
    //Tempo máximo em execução
    double time_max = atoi(argv[1]);
    //0-pontos dentro do circulo;1 - pontos totais
    unsigned long long local_points[2], global_points[2];
    local_points[0] = local_points[1] = global_points[0] = global_points[1] = 0;

    rc = MPI_Init(&argc, &argv);	

    if (rc != MPI_SUCCESS) {
        printf("Error starting MPI program.\n");
        MPI_Abort(MPI_COMM_WORLD, rc);
    }
	
	initial_time = MPI_Wtime();

    MPI_Get_processor_name(hostname, &len);

    MPI_Comm_size(MPI_COMM_WORLD, &numTasks);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    lapse_time = MPI_Wtime() - initial_time;

    uint seed = time(NULL)*rank;
    local_points[1] = 1;
    while (lapse_time < time_max) {

        double x_coord = (double) (rand_r(&seed)) / RAND_MAX;
        double y_coord = (double) (rand_r(&seed)) / RAND_MAX;

        if ((x_coord * x_coord + y_coord * y_coord) <= 1)
            local_points[0]++;

        lapse_time = MPI_Wtime() - initial_time;
        local_points[1]++;
	}
   
    MPI_Reduce(&local_points, &global_points, 2, MPI_UNSIGNED_LONG_LONG, MPI_SUM, 0, MPI_COMM_WORLD);

    if (rank == 0)
    {
        pi = (4*((double)(global_points[0]))/((double)(global_points[1])));
        // 1. Parse a JSON string into DOM.
        const char* json = "{\"numTasks\":0.0,\"time_max\":0.0,\"pointInside\":0.0,\"pointGlobal\":0.0,\"pi\":0.0}";
        rapidjson::Document d;
        d.Parse(json);

        // 2. Modify it by DOM.
        rapidjson::Value& s = d["numTasks"];
        s.SetInt(numTasks);
        rapidjson::Value& s2 = d["time_max"];
        s2.SetInt(time_max);        
        rapidjson::Value& s3 = d["pointInside"];
        s3.SetUint64(global_points[0]);
        rapidjson::Value& s4 = d["pointGlobal"];
        s4.SetUint64(global_points[1]);
        rapidjson::Value& s5 = d["pi"];
        s5.SetDouble(pi);                

        // 3. Stringify the DOM
        rapidjson::StringBuffer buffer;
        rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
        d.Accept(writer);

        printf("%s\n",buffer.GetString());


      /*  pi = (4*((double)(global_points[0]))/((double)(global_points[1])));
        printf("%s;%d;%llu;%llu;%1.50f\n",hostname,rank,global_points[0],global_points[1],pi);*/
    }

    MPI_Finalize();

    return 0;
}
